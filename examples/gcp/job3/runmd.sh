#!/bin/sh
#XQ instance_type g4dn.xlarge
#XQ image_name self:crossqueue--1594382260
#XQ stage dhfr.mdin
#XQ stage dhfr.prmtop
#XQ stage dhfr.crd

pip3 install pinda --user
pinda install amber 18
pmemd.cuda -i dhfr.mdin -c dhfr.crd -p dhfr.prmtop -o md.log -r dhfr.rst7
