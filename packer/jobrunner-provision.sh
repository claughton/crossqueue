# An example basic image to use with crossqueue, though any image that has
# Python >=3.6 and pip3 installed should work.
#
# In particular this image will have Docker and the nvidia container runtime
# installed so a) software can be installed using pinda, and b) the dockerized
# applications will be able to use the instance's GPU, if present.
#
sleep 30
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install -y python3-pip nfs-common zlib1g-dev 
# Install Docker
sudo apt install -y docker.io
sudo systemctl enable --now docker
sudo usermod -aG docker ${USER}
# Install Pinda
sudo pip3 install pinda
sudo pinda update
# Ensure local bin directory for pinda-installed packages is visible
mkdir -p $HOME/.local/bin
ln -s $HOME/.local/bin $HOME/bin
# Install default Nvidia driver
sudo apt install -y nvidia-driver-440
# Install Nvidia container runtime for Docker
curl -s -L https://nvidia.github.io/nvidia-container-runtime/gpgkey | \
  sudo apt-key add -
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
curl -s -L https://nvidia.github.io/nvidia-container-runtime/$distribution/nvidia-container-runtime.list | \
  sudo tee /etc/apt/sources.list.d/nvidia-container-runtime.list
sudo apt-get update
sudo apt-get install -y nvidia-container-runtime
sudo tee /etc/docker/daemon.json <<EOF
{
    "runtimes": {
        "nvidia": {
            "path": "/usr/bin/nvidia-container-runtime",
            "runtimeArgs": []
        }
    }
}
EOF
sudo pkill -SIGHUP dockerd
# Install ubuntu-drivers tool so drivers can be autoupdated at boot time
# depending on GPU configuration
sudo apt-get install -y ubuntu-drivers-common
