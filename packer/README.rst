######
Packer
######
The Crossqueue package contains `Packer <https://www.packer.io>`_ files to build images for use with **crossqueue**.
These instructions assume that you are able to access the ``packer/`` directory within the crossqueue source code.

For AWS, Azure, or GCP you can build a "base" image with `Docker <https://www.docker.com>`_ and GPU drivers installed,
and also `pinda <https://claughton.bitbucket.io>`_ so that containerised versions of various software packages
can easily be installed.

************
Instructions
************

1. Edit ``jobrunner-provision.sh`` if desired.
2. Edit ``jobrunner_<provider>.json`` and/or ``variables_<provider>.json`` as needed.
3. In the case of GCP, you will need to create a project, create an associated service account with Owner role, 
   and download a .json file with your service account credentials (see :doc:`Installation <installation>` for further details) and then set ::

        export GOOGLE_CLOUD_KEYFILE_JSON=<path_to_credentials_file>
        
   and enable the Compute Engine API from the GCP console if this has not previously been enabled on this project.
        
4. Generate a new "base" image for your chosen provider::

        packer build jobrunner_<provider>.json

   or, in the case where a variable file is used, ::

        packer build -var-file=variable_<provider>.json jobrunner_<provider>.json
        
   This may take up to half an hour to complete.
5. Use the name of the new image that is generated to set the ``node_defaults:image_name`` and
   ``terraform_config:mongodb_image_name`` values in your ``~/.xq/config.yaml`` file
