.. image:: https://readthedocs.org/projects/crossqueue/badge/?version=latest
   :target: https://crossqueue.readthedocs.io/en/latest/?badge=latest
   :alt: Documentation Status

##########
Crossqueue
##########

*Crossqueue* is part of `Project Crossbow <https://bitbucket.org/claughton/crossbow>`_. It allows you to create a pool
of instances in the cloud and then send jobs to them using a familiar batch queue type interface. *Crossqueue* handles
allocation of the jobs to instances, instance autoscaling, and file transfer.

Currently *Crossqueue* supports `Amazon Web Services <https://aws.amazon.com>`_,
`Microsoft Azure <https://azure.microsoft.com>`_ and `Google Cloud Platform <https://cloud.google.com>`_.

Documentation can be found at `ReadTheDocs <https://crossqueue.readthedocs.io/en/latest/>`_.

********
 Authors
********
* Christian Suess
* Charlie Laughton charles.laughton@nottingham.ac.uk
* Sam Cox

***************
Acknowlegements
***************
EPSRC Grant `EP/P011993/1 <https://gow.epsrc.ukri.org/NGBOViewGrant.aspx?GrantRef=EP/P011993/1>`_
