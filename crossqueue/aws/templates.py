node_defaults = {
    "image_owner": "099720109477",
    "image_name": "ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*",
    "instance_type": "t2.small"
}

terraform_initial_config = {
    "provider_id": "aws",
    "bucket_name": None,
    "mongodb_image_name": "ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*",
    "mongodb_image_owner": "099720109477",
    "mongodb_type": "t2.small",
    "mongodb_name": "mongodb",
    "mongodb_port": "27017",
    "node_names": [],
    "node_types": {},
    "image_names": {},
    "image_owners": {},
    "public_key_path": None,
    "region_name": None,
    "key_name": None,
    "sg_name": None
}

terraform_template = """ variable "region_name" {
  type = string
  description = "AWS region to use"
}

variable "provider_id" {
  type = string
  description = "Cloud provider - not used in template"
}

variable "node_names" {
  type = list
  description = "names of instances to launch"
  default = []
}

variable "node_types" {
  type = map
  description = "type of instance to launch"
  default = {}
}

variable "image_names" {
  type = map
  description = "image name used to create each instance"
  default = {}
}

variable "image_owners" {
  type = map
  description = "name of image owner"
  default = {}
}

variable "mongodb_name" {
  type = string
  description = "name for mongodb instance"
  default = "mongodb"
}

variable "mongodb_type" {
  type = string
  description = "mongodb instance type"
  default = "t2.small"
}

variable "mongodb_port" {
  type = string
  description = "mongodb port"
  default = "27017"
}

variable "mongodb_image_name" {
  type = string
  description = "image name used to create mongodb instance"
}

variable "mongodb_image_owner" {
  type = string
  description = "name of mongodb image owner"
}

variable "sg_name" {
  type = string
  description = "security group name"
}

variable "key_name" {
  type = string
  description = "key name"
}

variable "bucket_name" {
  type = string
  description = "Name of bucket used as queue system"
}

variable "public_key_path" {
  type = string
  description = "path to public key file"
}

provider "aws" {
  profile = "default"
  region  = var.region_name
}

data "aws_ami" "mongodb" {
  most_recent = true
  owners = [var.mongodb_image_owner]
  filter {
    name = "name"
    values = ["${var.mongodb_image_name}"]
  }
}

data "aws_ami" "node" {
  for_each = toset(var.node_names)
  most_recent = true
  owners = ["${var.image_owners[each.key]}"]
  filter {
    name = "name"
    values = ["${var.image_names[each.key]}"]
  }
}

resource "aws_key_pair" "terraform_ec2_key" {
  key_name = var.key_name
  public_key = file(var.public_key_path)
}

resource "aws_security_group" "allow_ssh" {
  name = var.sg_name
  description = "Allow ssh, web and mongodb  access from anywhere and full inter-worker comms"
  ingress {
    from_port = 22
    to_port   = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 80
    to_port   = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = var.mongodb_port
    to_port   = var.mongodb_port
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 0
    to_port   = 0
    protocol = "-1"
    self = true
  }
  egress {
    from_port = 0
    to_port = 0 
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  } 
} 

resource "aws_s3_bucket" "xq_bucket" {
  bucket = var.bucket_name
  acl = "private"
  force_destroy = true
}

resource "aws_instance" "mongodb" {

  ami = data.aws_ami.mongodb.id
  instance_type = var.mongodb_type
  key_name = var.key_name
  security_groups = ["${var.sg_name}"]
  iam_instance_profile = "EC2InstanceRole"
  tags = {
    Name = var.mongodb_name
  }
  user_data = <<-EOF
        #! /bin/sh
        sudo apt update
        sudo apt install gnupg
        wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | sudo apt-key add -
        echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.2.list
        sudo apt update
        sudo apt-get install -y mongodb-org
        sudo sed -i 's/127.0.0.1/0.0.0.0/' /etc/mongod.conf
        sudo systemctl start mongod
  EOF
} 

resource "aws_spot_instance_request" "node" {

  for_each = toset(var.node_names)
  ami = data.aws_ami.node[each.key].id
  instance_type = var.node_types[each.key]
  key_name = var.key_name
  security_groups = ["${var.sg_name}"]
  iam_instance_profile = "EC2InstanceRole"
  depends_on = [aws_instance.mongodb]
  tags = {
    name = each.key
  }
  user_data = <<-EOF
        #! /bin/sh
        sudo apt update
        sudo apt install -y python3-pip
        sudo pip3 install git+https://bitbucket.org/claughton/crossqueue.git
        sudo aws configure set default.region ${var.region_name} 
        sudo /usr/local/bin/jobrunner ${each.key} ${aws_instance.mongodb.private_ip} ${var.mongodb_port} > /tmp/jobrunner.log 2>&1 &
  EOF
  wait_for_fulfillment = true
  provisioner "local-exec" {
    command = "aws ec2 create-tags --resources ${self.spot_instance_id} --tags Key=Name,Value=${each.key}  --region ${var.region_name}"

    environment = {
      AWS_DEFAULT_REGION = "${var.region_name}"
    }
  }
}
// A variable for extracting the external ip of the mongodbs
output "mongodb-url" {
  value = "${aws_instance.mongodb.public_ip}:${var.mongodb_port}"
}

// A variable for extracting the external ip of the jobrunners
output "jobrunner-ip" {
  value = values(aws_spot_instance_request.node)[*].public_ip
}
"""
