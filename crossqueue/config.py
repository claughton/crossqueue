import os.path as op
import yaml

configdir = op.expanduser('~/.xq')
configfile = op.join(configdir, 'config.yaml')

def check_config():
    if not op.exists(configdir):
        raise RuntimeError(f'Error: cannot find configdir {configdir}. Have you run '
                           f'xq-init yet?')
    if not op.exists(configfile):
        raise RuntimeError(f'Error: cannot find config file at {configfile}. Have you run '
                           f'xq-init yet?')

def load_config(allow_nonexistent=False):
    try:
        check_config()
        with open(configfile) as f:
            config = yaml.load(f, Loader=yaml.SafeLoader)
        return config
    except (RuntimeError, FileNotFoundError) as err:
        if allow_nonexistent:
            return {}
        raise err

def save_config(config):
    with open(configfile, 'w') as f:
        yaml.dump(config, f)
