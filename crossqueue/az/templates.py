node_defaults = {
    "image_name": "XQJobrunnerImage",
    "instance_type": "Standard_B2ms"
}

terraform_initial_config = {
    "provider_id": "azure",
    "bucket_name": None,
    "mongodb_image_name": "XQJobrunnerImage",
    "mongodb_type": "Standard_B2ms",
    "mongodb_name": "mongodb",
    "mongodb_port": "27017",
    "node_names": [],
    "accelerator_types": {},
    "accelerator_counts": {},
    "node_types": {},
    "image_names": {},
    "subscription": None,
    "public_key_path": None,
    "location": "westeurope",
    "user_name": None,
    "resource_group_name": None
}

terraform_template = """variable "subscription"  {
  type = string
  description = "Azure subscription name"
}

variable "location" {
  type = string
  description = "Azure location to use"
}

variable "provider_id" {
  type = string
  description = "Cloud provider - not used in template"
}

variable "node_names" {
  type = list
  description = "names of instances to launch"
  default = []
}

variable "node_types" {
  type = map
  description = "type of instance to launch"
  default = {}
}

variable "image_names" {
  type = map
  description = "image name used to create each instance"
  default = {}
}

variable "accelerator_types" {
  type = map
  description = "type of accelerator to attach to each instance"
  default = {}
}

variable "accelerator_counts" {
  type = map
  description = "number of accelerators to attach to each instance"
  default = {}
}

variable "mongodb_name" {
  type = string
  description = "name for mongodb instance"
  default = "mongodb"
}

variable "mongodb_type" {
  type = string
  description = "mongodb instance type"
  default = "f1-micro"
}

variable "mongodb_port" {
  type = string
  description = "mongodb port"
  default = "27017"
}

variable "mongodb_image_name" {
  type = string
  description = "image name used to create mongodb instance"
}

variable "user_name" {
  type = string
  description = "user name on local machine"
}

variable "bucket_name" {
  type = string
  description = "Name of bucket used as queue system"
}

variable "public_key_path" {
  type = string
  description = "path to public key file"
}

variable "resource_group_name" {
    type = string
    description = "name of Azure resource group"
}

provider "azurerm" {
  features {}
  subscription_id = var.subscription
  skip_provider_registration = true
}

data "azurerm_resource_group" "rg" {
  name     = var.resource_group_name
}

resource "azurerm_virtual_network" "vnet" {
  name                = "xq-vnet"
  address_space       = ["10.0.0.0/16"]
  location            = data.azurerm_resource_group.rg.location
  resource_group_name = data.azurerm_resource_group.rg.name
}

resource "azurerm_subnet" "snet" {
  name                 = "xq-subnet"
  resource_group_name  = data.azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = ["10.0.2.0/24"]
  service_endpoints    = ["Microsoft.Sql", "Microsoft.Storage"]
}

resource "azurerm_public_ip" "scheduler-publicip" {
    name                         = "schedulerPublicIP"
    location                     = data.azurerm_resource_group.rg.location
    resource_group_name          = data.azurerm_resource_group.rg.name
    allocation_method            = "Dynamic"
}

resource "azurerm_network_security_group" "nsg" {
    name                = "xqNSG"
    location            = data.azurerm_resource_group.rg.location
    resource_group_name = data.azurerm_resource_group.rg.name

    security_rule {
        name                       = "SSH"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }

    security_rule {
        name                       = "mongo"
        priority                   = 1000
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_ranges    = ["80", var.mongodb_port]
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }
}

resource "azurerm_network_interface" "scheduler-nic" {
  name                = "scheduler-nic"
  location            = data.azurerm_resource_group.rg.location
  resource_group_name = data.azurerm_resource_group.rg.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.snet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.scheduler-publicip.id
  }
}

# Connect the security group to the network interface
resource "azurerm_network_interface_security_group_association" "scheduler" {
    network_interface_id      = azurerm_network_interface.scheduler-nic.id
    network_security_group_id = azurerm_network_security_group.nsg.id
}

data "azurerm_image" "image" {
  name                = var.mongodb_image_name
  resource_group_name = data.azurerm_resource_group.rg.name
}

resource "azurerm_linux_virtual_machine" "mongodb" {
  name                = var.mongodb_name
  resource_group_name = data.azurerm_resource_group.rg.name
  location            = data.azurerm_resource_group.rg.location
  size                = var.mongodb_type
  admin_username      = "adminuser"
  network_interface_ids = [
    azurerm_network_interface.scheduler-nic.id,
  ]

  admin_ssh_key {
    username   = "adminuser"
    public_key = file(var.public_key_path)
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_id = data.azurerm_image.image.id

  custom_data = base64encode(<<-EOF
        #! /bin/sh
        sudo apt update
        sudo apt install gnupg
        wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | sudo apt-key add -
        echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.2.list
        sudo apt update
        sudo apt-get install -y mongodb-org
        sudo sed -i 's/127.0.0.1/0.0.0.0/' /etc/mongod.conf
        sudo sed -i 's/27017/"${var.mongodb_port}"/' /etc/mongod.conf
        sudo systemctl start mongod 
  EOF
  )

  boot_diagnostics {
        storage_account_uri = azurerm_storage_account.storacc.primary_blob_endpoint
    }
}

data "http" "myip" {
  url = "http://ipv4.icanhazip.com"
}

resource "azurerm_storage_account" "storacc" {
  name                = var.bucket_name
  resource_group_name = data.azurerm_resource_group.rg.name

  location                 = data.azurerm_resource_group.rg.location
  account_tier             = "Standard"
  account_replication_type = "LRS"

  network_rules {
    default_action             = "Deny"
    ip_rules                   = ["100.0.0.1", "${chomp(data.http.myip.body)}"]
    virtual_network_subnet_ids = [azurerm_subnet.snet.id]
  }
}

resource "azurerm_storage_share" "share" {
  name                 = "xqfileshare"
  storage_account_name = azurerm_storage_account.storacc.name
  quota                = 50

  acl {
    id = "1"

    access_policy {
      permissions = "rwdl"
    }
  }
}

resource "azurerm_network_interface" "worker-nic" {
  for_each            = toset(var.node_names)
  name                = "${each.key}-nic"
  location            = data.azurerm_resource_group.rg.location
  resource_group_name = data.azurerm_resource_group.rg.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.snet.id
    private_ip_address_allocation = "Dynamic"
  }
}

# Connect the security group to the network interface
resource "azurerm_network_interface_security_group_association" "worker" {
    for_each                  = toset(var.node_names)
    network_interface_id      = azurerm_network_interface.worker-nic[each.key].id
    network_security_group_id = azurerm_network_security_group.nsg.id
}

resource "azurerm_linux_virtual_machine" "jobrunner" {
  for_each = toset(var.node_names)
  name = each.value
  size = var.node_types[each.key]
  resource_group_name = data.azurerm_resource_group.rg.name
  location            = data.azurerm_resource_group.rg.location
  admin_username      = "adminuser"
  network_interface_ids = [
    azurerm_network_interface.worker-nic[each.key].id,
  ]

  admin_ssh_key {
    username   = "adminuser"
    public_key = file(var.public_key_path)
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_id = data.azurerm_image.image.id

  custom_data = base64encode(<<-EOF
        #! /bin/sh
        sudo apt update
        sudo apt install -y python3-pip
        sudo apt-get remove unattended-upgrades
        sudo systemctl stop apt-daily.timer
        sudo systemctl disable apt-daily.timer
        sudo systemctl disable apt-daily.service
        sudo systemctl daemon-reload
        sudo pip3 install git+https://bitbucket.org/claughton/crossqueue.git
        sudo /usr/local/bin/jobrunner ${each.key} ${azurerm_linux_virtual_machine.mongodb.private_ip_address} ${var.mongodb_port} --az_conn_str "${azurerm_storage_account.storacc.primary_connection_string}" > /tmp/jobrunner.log 2>&1 &
  EOF
  )

  boot_diagnostics {
        storage_account_uri = azurerm_storage_account.storacc.primary_blob_endpoint
    }
}

// A variable for extracting the external ip of the mongodb
output "mongodb-url" {
  value = "${azurerm_linux_virtual_machine.mongodb.public_ip_address}:${var.mongodb_port}"
}

// A variable for extracting the external ip of the jobrunners
output "jobrunner-ip" {
  value = values(azurerm_linux_virtual_machine.jobrunner)[*].public_ip_address
}

output "storage-connection-string" {
  value = azurerm_storage_account.storacc.primary_connection_string
}
"""