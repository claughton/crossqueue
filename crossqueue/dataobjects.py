import time
from pymongo import MongoClient
from pymongo.errors import ConnectionFailure

class DataSource:
    def __init__(self, mongo_uri):
        self.client = MongoClient(mongo_uri)
        self.db = self.client['XQ']

    def is_connected(self):
        try:
            self.client.admin.command('ismaster')
        except ConnectionFailure:
            return False
        return True

    def document(self, collection, docname):
        return Document(self.db[collection], docname)

    def job(self, job_index=None):
        return Job(self.db, job_index=job_index)

    def node(self, node_id):
        return Node(self.db, node_id)

    def get_jobs(self, status=None):
        collection = self.db['jobs']
        if status is not None:
            if isinstance(status, str):
                status = [status]
            query = {'status': {"$in": status}}
            query_refs = collection.find(query)
        else:
            query_refs = collection.find()
        jobs = []
        for q in query_refs:
            j = self.job(job_index=q[u'job_index'])
            jobs.append(j)
        return jobs

    def get_nodes(self, status=None):
        collection = self.db[u'nodes']
        if status is not None:
            if isinstance(status, str):
                status = [status]
            query = {'status': {'$in': status}}
            query_refs = collection.find(query)
        else:
            query_refs = collection.find()
        nodes = []
        for q in query_refs:
            n = self.node(q[u'node_id'])
            nodes.append(n)
        return nodes

class Document:
    def __init__(self, collection, docname, cache_time=5):
        self.collection = collection
        self.filter = {'docname': docname}
        self.last_load = 0
        self.cache_time = cache_time
        self.load()
        if self.dict is None:
            self.collection.insert_one(self.filter)
            self.dict = self.collection.find_one(self.filter)
        self.updates = {}

    def load(self):
        #now = time.time()
        #if now - self.last_load > self.cache_time:
        #if now - self.last_load > self.cache_time:
        #     self.dict = self.collection.find_one(self.filter)    
        #     self.last_load = now
        self.dict = self.collection.find_one(self.filter)    
        #self.last_load = now

    def get(self, key):
        return self.dict.get(key)

    def put(self, key, value):
        self.dict[key] = value
        self.updates[key] = value
        self.updates['last_update'] = time.time()

    def update(self, dict=None):
        if dict is not None:
            self.updates.update(dict)
            self.updates['last_update'] = time.time()
        self.collection.update_one(self.filter, {"$set": self.updates})
        self.updates = {}

    def delete(self):
        self.collection.delete_one(self.filter)


class Job:
    def __init__(self, db, job_index=None):
        metadata = Document(db['metadata'], 'metadata')
        if job_index is None:
            last_job_index = metadata.get('last_job_index')
            if last_job_index is None:
                job_index = 0
            else:
                job_index = last_job_index + 1
            metadata.put('last_job_index', job_index)
            metadata.update()
            self.job_index = job_index
            self.status = u'nascent'
            self.submit_time = None
            self.start_time = None
            self.end_time = None
            self.node_id = None
            self.requirements = {}
            self.scriptfile = None
            self.last_update = time.time()
            self.returncode = None
            self.jobfiles = []
            self.job_ref = Document(db['jobs'], self.job_index)
            self.job_ref.update(self.to_dict())
            
        else:
            self.job_index = job_index
            self.job_ref = Document(db['jobs'], self.job_index)
            d =  self.job_ref.dict
            if d is None:
                raise ValueError('Error - job{} does not exist'.format(job_index))
            else:
                self.from_dict(d)
    
    def to_dict(self):
        d = {
            u'job_index': self.job_index,
            u'status': self.status,
            u'submit_time': self.submit_time,
            u'start_time': self.start_time,
            u'end_time': self.end_time,
            u'last_update': self.last_update,
            u'returncode': self.returncode,
            u'requirements': self.requirements,
            u'node_id': self.node_id,
            u'scriptfile': self.scriptfile,
            u'jobfiles': self.jobfiles
        }
        return d
    
    def __repr__(self):
        return "{}".format(self.to_dict())
    
    def reload(self):
        self.job_ref.load()
        d =  self.job_ref.dict
        self.from_dict(d)

    def allocate(self, node_id):
        self.node_id = node_id
        self.status = u'allocated'
        self.patch()

        
    def from_dict(self, d):
        self.job_index = d.get(u'job_index')
        self.status = d.get(u'status')
        self.submit_time = d.get(u'submit_time')
        self.start_time = d.get(u'start_time')
        self.end_time = d.get(u'end_time')
        self.last_update = d.get(u'last_update')
        self.returncode = d.get(u'returncode')
        self.requirements = d.get(u'requirements')
        self.node_id = d.get(u'node_id')
        self.scriptfile = d.get(u'scriptfile')
        self.jobfiles = d.get(u'jobfiles')
        
    def patch(self):
        self.last_update = time.time()
        self.job_ref.update(self.to_dict())

    def submit(self):
        if self.scriptfile is None:
            raise ValueError('Error - no script file defined')
        if self.status == 'nascent':
            self.status = 'queued'
            self.submit_time = time.time()
            self.patch()
        else:
            print('Error - cannot submit a {} job'.format(self.status))
        
    def delete(self):
        if not self.status in ['running', 'terminating']:
            self.job_ref.delete()
        else:
            print('Error - cannot delete a {} job'.format(self.status))
            
    def requeue(self):
        self.status = 'queued'
        self.patch()

    def hold(self):
        if self.status in ['queued', 'held']:
            self.status = 'held'
            self.patch()
        else:
            print('Error - cannot hold a {} job'.format(self.status))
    
    def release(self):
        if self.status == 'held':
            self.status = 'queued'
            self.patch()
        else:
            print('Error - cannot release a {} job'.format(self.status))
            
    def terminate(self):
        if self.status == 'running':
            self.status = 'terminating'
            self.patch()
        else:
            print('Error - cannot terminate a {} job'.format(self.status))

class Node:
    def __init__(self, db, node_id):
        self.node_id = node_id
        self.node_ref = Document(db['nodes'], node_id)
        d = self.node_ref.dict
        now = time.time()
        if u'status' not in d:
            self._status = u'unknown'
            self._last_heartbeat = now
            self._last_busy = now
            self._job_index = None
            self._properties = {}
            self.node_ref.update(self.to_dict())
        else:
            self.from_dict(d)
    
    def to_dict(self):
        d = {
            u'node_id': self.node_id,
            u'job_index': self._job_index,
            u'status': self._status,
            u'last_heartbeat': int(self._last_heartbeat),
            u'properties': self._properties,
            u'last_busy': int(self._last_busy)
        }
        return d
    
    def __repr__(self):
        return "{}".format(self.to_dict())
    
    def from_dict(self, d):
        self.node_id = d.get(u'node_id')
        self._status = d.get(u'status')
        self._properties = d.get(u'properties')
        try:
            self._last_heartbeat = float(d.get(u'last_heartbeat'))
        except:
            print(d)
            raise
        self._last_busy = float(d.get(u'last_busy'))
        self._job_index = d.get(u'job_index')
        
    def reload(self):
        self.node_ref.load()
        d = self.node_ref.dict
        self.from_dict(d)
        
    def allocate(self, job_index):
        self._job_index = job_index
        self._status = u'allocated'
        self.node_ref.update(self.to_dict())
        self.reload()

    @property
    def status(self):
        now = int(time.time())
        self.node_ref.load()
        d = self.node_ref.dict
        self.from_dict(d)
        if self._status == u'launching' and now - self.last_heartbeat > 1000:
            self._status = u'dead'
        elif self._status != u'launching' and now - self.last_heartbeat > 60:
            self._status = u'dead'
        else:
            if self._status == u'busy':
                if now - self._last_busy > 60:
                    self._status = u'idle'
            if self._status == u'dead':
                if now - self.last_busy <= 60:
                    self._status = u'busy'
                else:
                    self._status = u'unknown'
        self.node_ref.update(self.to_dict())
        return self._status
    
    @status.setter
    def status(self, status):
        self._status = status
        self.node_ref.update(self.to_dict())
            
    @property
    def last_heartbeat(self):
        self.node_ref.load()
        d = self.node_ref.dict
        self.from_dict(d)
        return self._last_heartbeat
    
    @last_heartbeat.setter
    def last_heartbeat(self, time):
        self._last_heartbeat = time
        self.node_ref.update(self.to_dict())
        
    @property
    def properties(self):
        self.node_ref.load()
        d = self.node_ref.dict
        self.from_dict(d)
        return self._properties
    
    @properties.setter
    def properties(self, properties):
        self._properties = properties
        self.node_ref.update(self.to_dict())
        
    @property
    def last_busy(self):
        self.node_ref.load()
        d = self.node_ref.dict
        self.from_dict(d)
        return self._last_busy
    
    @last_busy.setter
    def last_busy(self, time):
        self._last_busy = time
        self.node_ref.update(self.to_dict())
            
    @property
    def job_index(self):
        self.node_ref.load()
        d = self.node_ref.dict
        self.from_dict(d)
        return self._job_index
    
    @job_index.setter
    def job_index(self, job_index):
        self._job_index = job_index
        self.node_ref.update(self.to_dict())
            
    def delete(self):
        if self.status in [u'dead', u'terminated', u'unknown', u'idle']:
            self.node_ref.delete()
        else:
            print('Error - cannot delete a {} node'.format(self.status))
