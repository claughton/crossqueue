import threading
import itertools
import subprocess
import time
import os.path as op
import os
import sys
import json

def run_progress_bar(finished_event):
    """
    Helper function to display a progress bar while awaiting an event.
    """
    chars = itertools.cycle(r'-\|/')
    while not finished_event.is_set():
        sys.stdout.write('\rWorking ' + next(chars))
        sys.stdout.flush()
        finished_event.wait(0.2)


def create(deploymentdir, template, config):
    """
    Initialise the terraform directory with the supplied template and config file. Then
    create and return a Deployment.
    """
    if op.exists(deploymentdir):
        raise OSError('Error: directory {} already exists'.format(deploymentdir))
    os.makedirs(deploymentdir)
    templatefile = op.join(deploymentdir, 'template.tf')
    configfile = op.join(deploymentdir, 'terraform.tfvars.json')
    with open(templatefile, 'w') as f:
        f.write(template)
    with open(configfile, 'w') as f:
        json.dump(config, f, indent=2)
    return Deployment(deploymentdir)

class Deployment:
    """
    A Terraform deployment object, with functions to initialise, apply, destroy,
    and get outputs. This is essentially a wrapper to provide programmatic access to
    the contents of the terraform directory, and call terraform commands.
    """
    def __init__(self, deploymentdir, progress_bar=False):
        self.deploymentdir = deploymentdir
        self.templatefile = op.join(deploymentdir, 'template.tf')
        self.configfile = op.join(deploymentdir, 'terraform.tfvars.json')
        self.statefile = op.join(deploymentdir, 'terraform.tfstate')
        if not op.exists(self.configfile) or not op.exists(self.templatefile):
            raise ValueError('Error - {} does not contain a valid terraform deployment'.format(self.deploymentdir))
        self._spr('terraform init', progress_bar=progress_bar)

    def _spr(self, cmd, progress_bar=False, max_retries=2, retry_interval=5):
        success = False
        ntries = 0
        if progress_bar:
            finished_event = threading.Event()
            progress_bar_thread = threading.Thread(target=run_progress_bar, args=(finished_event,))
            progress_bar_thread.start()
        while not success and ntries < max_retries:
            result = subprocess.run(cmd, shell=True, capture_output=True, 
                                cwd=self.deploymentdir)
            if result.returncode == 0:
                self.status = 'OK'
                success = True
            else:
                self.status = 'Failed'
                time.sleep(retry_interval)
            ntries += 1
        if progress_bar:
            finished_event.set()
            progress_bar_thread.join()

        self.stdout = result.stdout
        self.stderr = result.stderr

    def apply(self, config=None, progress_bar=False, max_retries=2, retry_interval=5):
        if config is not None:
            with open(self.configfile, 'w') as f:
                json.dump(config, f, indent=2)
        self._spr('terraform apply -no-color -auto-approve',
                 max_retries=max_retries, retry_interval=retry_interval,
                 progress_bar=progress_bar)

    def config(self):
        with open(self.configfile) as f:
            return json.load(f)

    def outputs(self,  max_retries=2, retry_interval=5, progress_bar=False):
        self._spr('terraform output -json', 
                 max_retries=max_retries, retry_interval=retry_interval,
                 progress_bar=progress_bar)
        if self.status == 'OK':
            return json.loads(self.stdout.decode())
        else:
            return None

    def destroy(self, max_retries=2, retry_interval=5, progress_bar=False):
        self._spr('terraform destroy -auto-approve -no-color', 
                 max_retries=max_retries, retry_interval=retry_interval,
                 progress_bar=progress_bar)

