node_defaults = {
    "image_name": "ubuntu-2004-focal-v20200423",
    "instance_type": "f1-micro",
    "accelerator_type": "nvidia-tesla-t4",
    "accelerator_count": "0"
}

terraform_initial_config = {
    "provider_id": "gcp",
    "bucket_name": None,
    "mongodb_image_name": "ubuntu-2004-focal-v20200423",
    "mongodb_type": "f1-micro",
    "mongodb_name": "mongodb",
    "mongodb_port": "27017",
    "node_names": [],
    "accelerator_types": {},
    "accelerator_counts": {},
    "node_types": {},
    "image_names": {},
    "project_name": None,
    "public_key_path": None,
    "region_name": None,
    "user_name": None,
    "zone_name": None
}

terraform_template = """ variable "project_name"  {
  type = string
  description = "GCP project name"
}

variable "region_name" {
  type = string
  description = "GC region to use"
}

variable "zone_name" {
  type = string
  description = "GC zone to use"
}

variable "provider_id" {
  type = string
  description = "Cloud provider - not used in template"
}

variable "node_names" {
  type = list
  description = "names of instances to launch"
  default = []
}

variable "node_types" {
  type = map
  description = "type of instance to launch"
  default = {}
}

variable "image_names" {
  type = map
  description = "image name used to create each instance"
  default = {}
}

variable "accelerator_types" {
  type = map
  description = "type of accelerator to attach to each instance"
  default = {}
}

variable "accelerator_counts" {
  type = map
  description = "number of accelerators to attach to each instance"
  default = {}
}

variable "mongodb_name" {
  type = string
  description = "name for mongodb instance"
  default = "mongodb"
}

variable "mongodb_type" {
  type = string
  description = "mongodb instance type"
  default = "f1-micro"
}

variable "mongodb_port" {
  type = string
  description = "mongodb port"
  default = "27017"
}

variable "mongodb_image_name" {
  type = string
  description = "image name used to create mongodb instance"
}

variable "user_name" {
  type = string
  description = "user name on local machine"
}

variable "bucket_name" {
  type = string
  description = "Name of bucket used as queue system"
}

variable "public_key_path" {
  type = string
  description = "path to public key file"
}

provider "google" {
  project = var.project_name
  region  = var.region_name
  zone    = var.zone_name
}

resource "google_compute_firewall" "default" {
  name    = "xq-firewall"
  network = google_compute_network.default.name

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["22", var.mongodb_port]
  }

}

resource "google_compute_network" "default" {
  name = "xq-network"
}

resource "google_storage_bucket" "xq_bucket" {
  name = var.bucket_name
  location = "EU"
  force_destroy = true
}

resource "google_compute_instance" "mongodb" {
  name = var.mongodb_name
  machine_type = var.mongodb_type
  allow_stopping_for_update = "true"

  metadata = {
    ssh-keys = "${var.user_name}:${file(var.public_key_path)}"
    }

  metadata_startup_script = <<-EOF
        #! /bin/sh
        sudo apt update
        sudo apt install gnupg
        wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | sudo apt-key add -
        echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.2.list
        sudo apt update
        sudo apt-get install -y mongodb-org
        sudo sed -i 's/127.0.0.1/0.0.0.0/' /etc/mongod.conf
        sudo sed -i 's/27017/"${var.mongodb_port}"/' /etc/mongod.conf
        sudo systemctl start mongod 
  EOF

  boot_disk {
    initialize_params {
      image = var.mongodb_image_name
    }
  }

  network_interface {
    network = google_compute_network.default.name
    access_config {
    }
  }

  service_account {
    scopes = ["cloud-platform"]
  }
} 

resource "google_compute_instance" "jobrunner" {
  for_each = toset(var.node_names)
  name = each.value
  machine_type = var.node_types[each.key]
  allow_stopping_for_update = "true"

  metadata = {
    ssh-keys = "${var.user_name}:${file(var.public_key_path)}"
    }

  metadata_startup_script = <<-EOF
        #! /bin/sh
        sudo apt update
        sudo apt install -y python3-pip
        sudo pip3 install git+https://bitbucket.org/claughton/crossqueue.git
        sudo /usr/local/bin/jobrunner ${each.key} ${google_compute_instance.mongodb.network_interface.0.network_ip} ${var.mongodb_port} > /tmp/jobrunner.log 2>&1 &
  EOF

  scheduling {
    preemptible = "false"
    on_host_maintenance = "TERMINATE"
    }

  guest_accelerator {
    type = var.accelerator_types[each.key]
    count = var.accelerator_counts[each.key]
  }

  boot_disk {
    initialize_params {
      image = var.image_names[each.key]
    }
  }

  network_interface {
    network = google_compute_network.default.name
    access_config {
    }
  }

  service_account {
    scopes = ["cloud-platform"]
  }
}
// A variable for extracting the external ip of the mongodb
output "mongodb-url" {
  value = "${google_compute_instance.mongodb.network_interface.0.access_config.0.nat_ip}:${var.mongodb_port}"
}
// A variable for extracting the external ip of the jobrunners
output "jobrunner-ip" {
  value = values(google_compute_instance.jobrunner)[*].network_interface.0.access_config.0.nat_ip
}
"""
