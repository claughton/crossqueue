#!/usr/bin/env python
"""
This file defines all the commands available, along with some helper functions. These
functions should be accessed by the user through the ``xq`` shell command.
"""
import time
import os.path as op
import logging
from fs.osfs import OSFS
from fs.path import abspath
from fs.copy import copy_fs_if_newer, copy_file_if_newer, copy_structure
from . import dataobjects
from . import terraform
from .daemon import daemon_running, stop_daemon, start_daemon

from .config import load_config, save_config, configdir

logger = logging.getLogger('xq_daemon')
SHARE_NAME = 'xqfileshare'

def allocate():
    """
    Allocates all jobs to nodes. If a job is running on a dead node, it gets
    reallocated. Otherwise, if it is allocated then it gets reallocated, and if it is
    not yet allocated then allocated. In this way, all jobs that either have not yet
    started, or have ended up on a dead node, are eligible for allocation.
    """

    if not daemon_running():
        print('Warning: the crossqueue daemon is not running')

    purge()

    config = load_config()
    data_source = dataobjects.DataSource(config['mongodb_uri'])
    if not data_source.is_connected():
        logger.warning('Allocate: Cannot connect to database')
        return
    deadnodes = data_source.get_nodes(status=u'dead')
    current_jobs = data_source.get_jobs(status=[u'running', u'allocated'])
    
    for job in current_jobs:
        if job.node_id in deadnodes:
            job.requeue()
            logger.info('allocate: re-queueing job {}'.format(job.job_index))

    allocated_jobs = data_source.get_jobs(status= u'allocated')
    for job in allocated_jobs:
        job.requeue()
        logger.info('allocate: reallocating job {}'.format(job.job_index))
    
    queued_jobs = data_source.get_jobs(status= u'queued')
    queued_jobs.sort(key=lambda job: -len(job.requirements))
    idle_nodes = data_source.get_nodes(status=u'idle')
    if len(idle_nodes) == 0 or len(queued_jobs) == 0:
        return
    logger.info('allocate: {} jobs queued'.format(len(queued_jobs)))
    logger.info('allocate: {} nodes free'.format(len(idle_nodes)))
    for job in queued_jobs:
        n_idle = len(idle_nodes)
        if n_idle == 0:
            return
        allocated = False
        i = 0
        while i < n_idle and not allocated:
            logger.info('allocate: looking to place job {}'.format(job.job_index))
            node = idle_nodes[i]
            matched = True
            for r in job.requirements:
                if node.properties.get(r) != job.requirements[r]:
                    matched = False
                
            if matched:
                job.allocate(node.node_id)
                node.allocate(job.job_index)
                idle_nodes.pop(i)
                logger.info('allocate: job {} requirements {} allocated to node {} properties {}'.format(job.job_index, job.requirements, node.node_id, node.properties))
                allocated = True
            else:
                i += 1
        if not allocated:
            logger.info('allocate: unable to allocate job {}'.format(job.job_index))


def configure(kv):
    """
    Get or set parameters of the xq system. Use the syntax ``xq configure max_nodes=3``
    for example to set the parameters, or ``xq configure max_nodes`` to print the
    configuration value.
    """
    if '=' in kv:
        setting = True
        k, v = kv.split('=')
    else:
        k = kv
        v = None
        setting = False
    config = load_config()
    found = False
    if k in config:
        if setting:
            config[k] = v
        else:
            v = config[k]
        found = True
    for section in ['node_defaults', 'terraform_config']:
        if k in config[section]:
            if setting:
                config[section][k] = v
            else:
                v = config[section][k]
            found = True
    if not found:
        print("Error - can't find parameter {}".format(k))
    else:
        save_config(config)
        print("{}={}".format(k, v)) 

def shutdown():
    """
    Shutdown and clean up entirely
    """
    if daemon_running():
        print('Stopping daemon...')
        stop_daemon()

    config = load_config()
    deployment = terraform.Deployment(op.join(configdir, 'terraform'))
    config['terraform_config']['node_names'] = []
    config['terraform_config']['node_types'] = {}
    config['terraform_config']['image_names'] = {}
    if config['terraform_config']['provider_id'] == 'gcp':
        config['terraform_config']['accelerator_types'] = {}
        config['terraform_config']['accelerator_counts'] = {}
    save_config(config)

    data_source = dataobjects.DataSource(config['mongodb_uri'])
    if data_source.is_connected():
        jobs = data_source.get_jobs()
        print('Cancelling jobs...')
        cancel([j.job_index for j in jobs])
        time.sleep(10)
    print('Terminating nodes...')
    rescale()
    print('Deleting cloud infrastructure...')
    deployment.destroy(progress_bar=True)
    if deployment.status == 'OK':
        config['mongodb_uri'] = None
        save_config(config)
        print('All done without errors.')
    else:
        print(deployment.stdout)
        print(deployment.stderr)
        print('Warning: There were errors in the clean-up and some cloud resources may remain.')

def start():
    """
    Build the XQ infrastructure from the config built using the ``xq-init`` script. This
    can be called from the command line.
    """
    restart(start=True)

def restart(start=False):
    """
    Rebuild the XQ infrastructure from the config built using the ``xq-init`` script.
    This can be called from the command line.
    """
    if start:
        print('Creating cloud infrastructure...')
    else:
        print('Recreating cloud infrastructure...')
    config = load_config()
    deployment = terraform.Deployment(op.join(configdir, 'terraform'))
    dconf = config['terraform_config']
    deployment.apply(dconf, progress_bar=True)
    if deployment.status == "OK":
        config = load_config()
        data = deployment.outputs()
        while data['mongodb-url']['value'] is None:
            print('waiting for mongodb to launch...')
            time.sleep(10)
            data = deployment.outputs()
        config['mongodb_uri'] = 'mongodb://{}/'.format(data['mongodb-url']['value'])
        print('mongodb uri = {}'.format(config['mongodb_uri']))
        if config['terraform_config']['provider_id'] == 'azure':
            config['AZURE_STORAGE_CONNECTION_STRING'] = data['storage-connection-string']['value']
        save_config(config)
        data_source = dataobjects.DataSource(config['mongodb_uri'])
        while not data_source.is_connected():
            print('waiting for database connection...')
            time.sleep(10)
        print('All done without errors.')
        print('Starting daemon...')
        start_daemon()
    else:
        print(deployment.stdout)
        print(deployment.stderr)
        print('Warning: There were errors restarting and some cloud resources may be missing.')

def azure_copy_fs_to_local_if_newer(conn_str, sharename, local_fs):
    """
    Helper function for Azure filesystem: walk over all remote directories and files
    in the supplied share, and copy files to the local filesystem if their timestamp
    is newer than the local copy.
    """
    from azure.storage.fileshare import ShareClient as AzShare, ShareFileClient as AzFile
    from datetime import datetime, timezone

    index = {}
    for path, info in local_fs.walk.info(namespaces=['details']):
        index[path] = info.modified.timestamp()
    remote_index = azure_walk_file_hierarchy(AzShare.from_connection_string(conn_str,
                                                                            share_name=sharename),
                                             0, "")

    for key in remote_index:
        if (abspath(key) not in index) or (
                abspath(key) in index
                and remote_index[key]['last_modified'] >
                datetime.fromtimestamp(index[abspath(key)], timezone.utc)):
            # copy remote to local
            if remote_index[key]['is_directory']:
                local_fs.makedir(key, recreate=True)
            else:
                with open(op.join(local_fs.root_path, key[1:]), "wb") as filename:
                    stream = AzFile.from_connection_string(conn_str=conn_str,
                                                           share_name=sharename,
                                                           file_path=key[1:]).download_file()
                    filename.write(stream.readall())

def downsync():
    """
    Manually trigger the filesystem sync from the command line.
    """
    config = load_config()
    if config['terraform_config']['provider_id'] == 'aws':
        from fs_s3fs import S3FS as BUCKETFS
    elif config['terraform_config']['provider_id'] == 'gcp':
        from fs_gcsfs import GCSFS as BUCKETFS
    elif config['terraform_config']['provider_id'] == 'azure':
        pass
    else:
        raise NotImplementedError(
            config['terraform_config']['provider_id'] + ' provider is not implemented')

    bucket_name = config['terraform_config']['bucket_name']
    local_root_dir = config['local_root_dir']

    local_fs = OSFS(local_root_dir)
    logger.info('sync: Syncing files')
    if config['terraform_config']['provider_id'] in ['aws', 'gcp']:
        bucket_fs = BUCKETFS(bucket_name=bucket_name)
        copy_fs_if_newer(bucket_fs, local_fs)
    elif config['terraform_config']['provider_id'] == 'azure':
        conn_str = config['AZURE_STORAGE_CONNECTION_STRING']
        azure_copy_fs_to_local_if_newer(conn_str, SHARE_NAME, local_fs)
    else:
        raise NotImplementedError(
            config['terraform_config']['provider_id'] + ' provider is not implemented')

def azure_walk_file_hierarchy(client, depth, prefix="", index={}, tree=False):
    """
    Helper function for Azure filesystem: create a nested dictionary mirroring
    the directory structure of the existing Azure directory. This recurses through
    the filesystem to build a structure of all the subdirectories. If ``tree=True``,
    this prints the structure in a manner similar to the ``tree`` command.
    """
    from azure.storage.fileshare import ShareClient as AzShare, ShareDirectoryClient as AzDir

    separator = '│   '
    for idx, item in enumerate(client.list_directories_and_files()):
        if tree:
            if idx == len(list(client.list_directories_and_files()))-1:
                print(separator * depth + '└── '  + item['name'])
            else:
                print(separator * depth + '├── '  + item['name'])
        # If depth == 0 then we don't want a preceding '/', and prefix will be ""
        key = prefix + max(depth, 1) * '/' + item['name']
        if item['is_directory']:
            # Directory, so recurse. AzDir and AzShare have different functions to get
            # (sub)directories
            if isinstance(client, AzDir):
                assert depth > 0
                sub_client = client.get_subdirectory_client(directory_name=item['name'])
            else:
                assert isinstance(client, AzShare)
                assert depth == 0
                sub_client = client.get_directory_client(directory_path=item['name'])
            index[key] = {'last_modified': sub_client.get_directory_properties()['last_modified'],
                          'is_directory': True}
            index = azure_walk_file_hierarchy(sub_client, depth + 1, key, index, tree)
        else:
            # This is a file
            index[key] = {'last_modified': client.get_file_client(item['name']).get_file_properties()['last_modified'],
                          'is_directory': False}
    return index

def downsyncd(index=None):
    """
    Sync remote file system to local file system. This is largely one-way, but if you delete a local file
    then it will be deleted on the remote as well (so deletion is local->remote).
    One-way sync files from remote FS to local FS. If no index is supplied, then a new index of the local
    file system is created, and all remote files synced to the local. If an index _is_ supplied, it is used
    to not repeatedly copy files from the remote that have been deleted on the local.
    """
    config = load_config()
    if config['terraform_config']['provider_id'] == 'aws':
        from fs_s3fs import S3FS as BUCKETFS
    elif config['terraform_config']['provider_id'] == 'gcp':
        from fs_gcsfs import GCSFS as BUCKETFS
    elif config['terraform_config']['provider_id'] == 'azure':
        from azure.storage.fileshare import ShareClient as AzShare, \
            ShareServiceClient as AzService, ShareDirectoryClient as AzDir, \
            ShareFileClient as AzFile
    else:
        raise NotImplementedError(
            config['terraform_config']['provider_id'] + ' provider is not implemented')
    bucket_name = config['terraform_config']['bucket_name']
    local_root_dir = config['local_root_dir']
    local_fs = OSFS(local_root_dir)
    if config['terraform_config']['provider_id'] in ['aws', 'gcp']:
        bucket_fs = BUCKETFS(bucket_name=bucket_name)
    elif config['terraform_config']['provider_id'] == 'azure':
        conn_str = config['AZURE_STORAGE_CONNECTION_STRING']
        share = AzShare.from_connection_string(conn_str=conn_str, share_name=SHARE_NAME)
        if SHARE_NAME not in [share['name'] for share in
                             AzService.from_connection_string(conn_str=conn_str).list_shares()]:
            share.create_share()
    else:
        raise NotImplementedError(
            config['terraform_config']['provider_id'] + ' provider is not implemented')
        
    if index is None:
        # This is a 'new' sync - build a full image of the local directory structure with modified timestamps,
        # for use in later calls.
        index = {}
        for path, info in local_fs.walk.info(namespaces=['details']):
            index[path] = info.modified.timestamp()
        # print('local index: ', index)
    else:
        # This builds on a previous sync - build a full image of the local directory structure with modified 
        # timestamps in `new_index`, and then compare the two indices to remove any remote files that have been
        # removed locally since the last index was created. In sum, this means that files on the remote are deleted
        # if they were previously on the local but no longer.
        new_index = {}
        for path, info in local_fs.walk.info(namespaces=['details']):
            new_index[path] = info.modified.timestamp()
        if config['terraform_config']['provider_id'] in ['aws', 'gcp']:

            for key in index:
                if key not in new_index:
                    ''' By here, file was previously here but no longer, so has been deleted locally '''
                    if bucket_fs.exists(key):
                        ''' By here, file does exist on remote, so remove it from remote'''
                        info = bucket_fs.getinfo(key)
                        if info.is_dir:
                            logger.debug('removing directory {}'.format(key))
                            bucket_fs.removetree(key)
                        else:
                            logger.debug('removing file {}'.format(key))
                            bucket_fs.remove(key)
        elif config['terraform_config']['provider_id'] == 'azure':
            remote_index = azure_walk_file_hierarchy(AzShare.from_connection_string(
                conn_str, share_name=SHARE_NAME), 0, "")
            for key in index:
                if key not in new_index:
                    ''' By here, file was previously here but no longer, so has been deleted locally '''
                    if key in remote_index:
                        ''' By here, file does exist on remote, so remove it from remote'''
                        if remote_index[key]['is_directory']:
                            logger.debug('removing directory {}'.format(key))
                            AzDir.from_connection_string(conn_str=conn_str,
                                                         share_name=SHARE_NAME,
                                                         directory_path=key[1:]).delete_directory()
                        else:
                            logger.debug('removing file {}'.format(key))
                            AzFile.from_connection_string(conn_str=conn_str,
                                                           share_name=SHARE_NAME,
                                                           file_path=key[1:]).delete_file()

        else:
            raise NotImplementedError(
                config['terraform_config'][
                    'provider_id'] + ' provider is not implemented')
        index = new_index

    logger.debug('sync: Syncing files')
    # Copy all files from remote to local if they are newer on the remote.
    if config['terraform_config']['provider_id'] in ['aws', 'gcp']:
        copy_fs_if_newer(bucket_fs, local_fs)
    elif config['terraform_config']['provider_id'] == 'azure':
        azure_copy_fs_to_local_if_newer(conn_str, SHARE_NAME, local_fs)
    else:
        raise NotImplementedError(
            config['terraform_config']['provider_id'] + ' provider is not implemented')
    return index

def remove(localfiles):
    """
    Remove the supplied file from the local filesystem and the bucket filesystem. Can
    be called from the command line.
    """
    config = load_config()
    if config['terraform_config']['provider_id'] == 'aws':
        from fs_s3fs import S3FS as BUCKETFS
    elif config['terraform_config']['provider_id'] == 'gcp':
        from fs_gcsfs import GCSFS as BUCKETFS
    elif config['terraform_config']['provider_id'] == 'azure':
        from azure.storage.fileshare import ShareFileClient as AzFile
        from azure.core.exceptions import ResourceNotFoundError
    else:
        raise NotImplementedError(
            config['terraform_config']['provider_id'] + ' provider is not implemented')
    bucket_name = config['terraform_config']['bucket_name']
    local_root_dir = config['local_root_dir']

    local_fs = OSFS(local_root_dir)
    if config['terraform_config']['provider_id'] in ['aws', 'gcp']:
        bucket_fs = BUCKETFS(bucket_name=bucket_name)
    elif config['terraform_config']['provider_id'] == 'azure':
        conn_str = config['AZURE_STORAGE_CONNECTION_STRING']
    else:
        raise NotImplementedError(
            config['terraform_config']['provider_id'] + ' provider is not implemented')

    for lf in localfiles:
        if not op.exists(lf):
            print('Error: {} - no such file'.format(lf))
        elif op.isdir(lf):
            print('Error: {} - cannot delete directories'.format(lf))
        else:
            rp = op.relpath(lf, local_root_dir)
            if config['terraform_config']['provider_id'] in ['aws', 'gcp']:
                if bucket_fs.exists(rp):
                    bucket_fs.remove(rp)
            elif config['terraform_config']['provider_id'] == 'azure':
                try:
                    AzFile.from_connection_string(conn_str=conn_str,
                                                  share_name=SHARE_NAME,
                                                  file_path=rp).delete_file()
                except ResourceNotFoundError:
                    pass
            else:
                raise NotImplementedError(
                    config['terraform_config'][
                        'provider_id'] + ' provider is not implemented')
            local_fs.remove(rp)

def nodestat():
    """
    Print all nodes and their status, current/last job, and last update time. Can be
    called from the command line.
    """
    if not daemon_running():
        print('Warning: the crossqueue daemon is not running')

    config = load_config()
    data_source = dataobjects.DataSource(config['mongodb_uri'])
    if not data_source.is_connected():
        print('Cannot connect to database')
        return
    nodes = data_source.get_nodes()
    if len(nodes) == 0:
        print('No nodes in the pool')
        return
    for node in nodes:
        status = node.status
        job_index = node.job_index
        last_heartbeat = node.last_heartbeat
        ctime = time.ctime(int(last_heartbeat))
        if status in [u'allocated', u'busy', u'terminating']:
            print('node {}: {} current job: {} last_update {}'.format(node.node_id, status, job_index, ctime))
        else:
            print('node {}: {}    last job: {} last_update {}'.format(node.node_id, status, job_index, ctime))

def cancel(job_indices):
    """
    Cancel the supplied jobs. If the job is currently running, this signals the
    worker node to cancel the job. If the job is not currently running, its status
    is set to 'cancelled' and it will be deleted at the next daemon cycle. This can be
    called from the command line.
    """
    if not daemon_running():
        print('Warning: the crossqueue daemon is not running')

    config = load_config()
    data_source = dataobjects.DataSource(config['mongodb_uri'])
    if not data_source.is_connected():
        print('Warning: cannot connect to database')
        return
    jobs = data_source.get_jobs()
    for job in jobs:
        job_index = job.job_index
        if job_index in job_indices:
            if job.status != u'completed':
                if job.status == u'running':
                    job.status = u'cancelling'
                    print('job {} signalled to cancel'.format(job_index))
                elif job.status in [u'queued', u'held']:
                    job.status = u'cancelled'
                    job.end_time = time.time()
                    print('job {} cancelled'.format(job_index))
                job.patch()
            else:
                print('Error - job {} has already completed'.format(job_index))

def delete(job_indices):
    """
    Delete the supplied jobs. If the job is currently running, this has no effect.
    This can be called from the command line.
    """
    if not daemon_running():
        print('Warning: the crossqueue daemon is not running')

    config = load_config()
    data_source = dataobjects.DataSource(config['mongodb_uri'])
    if not data_source.is_connected():
        print('Warning: cannot connect to database')
        return
    jobs = data_source.get_jobs()
    for job in jobs:
        job_index = job.job_index
        if job_index in job_indices:
            if not job.status == 'running':
                job.delete()
                print('Job {} deleted'.format(job_index))
            else:
                print('Error: job {} is running, use cancel first'.format(job_index))

def hold(job_indices):
    """
    Hold a job by index. A held job will not be allocated to a node. Use ``xq
    release`` to allow the job to be allocated.
    """
    if not daemon_running():
        print('Warning: the crossqueue daemon is not running')

    config = load_config()
    data_source = dataobjects.DataSource(config['mongodb_uri'])
    if not data_source.is_connected():
        print('Warning: cannot connect to database')
        return
    for job in data_source.get_jobs():
        job_index = job.job_index
        if job.job_index in job_indices:
            if job.status == 'queued':
                job.status = 'held'
                job.patch()
                print('job {} held'.format(job_index))
            elif job.status == 'held':
                print('job {} already held'.format(job_index))
            elif job.status == 'running':
                print('Error - job {} is already running'.format(job_index))
            else:
                print('Error - job {} has already completed'.format(job_index))

def filestat():
    """
    List contents of the bucket file system in a similar format to the output of the
    ``tree`` shell command.
    """
    if not daemon_running():
        print('Warning: the crossqueue daemon is not running')

    config = load_config()
    if config['terraform_config']['provider_id'] == 'aws':
        from fs_s3fs import S3FS as BUCKETFS
    elif config['terraform_config']['provider_id'] == 'gcp':
        from fs_gcsfs import GCSFS as BUCKETFS
    elif config['terraform_config']['provider_id'] == 'azure':
        from azure.storage.fileshare import ShareClient as AzShare
    else:
        raise NotImplementedError(
            config['terraform_config']['provider_id'] + ' provider is not implemented')
    bucket_name = config['terraform_config']['bucket_name']

    if config['terraform_config']['provider_id'] in ['aws', 'gcp']:
        bucket_fs = BUCKETFS(bucket_name=bucket_name)
        print(bucket_fs.tree())
    elif config['terraform_config']['provider_id'] == 'azure':
        conn_str = config['AZURE_STORAGE_CONNECTION_STRING']
        azure_walk_file_hierarchy(AzShare.from_connection_string(conn_str,
                                                                 share_name=SHARE_NAME),
                                  0, "", tree=True)
    else:
        raise NotImplementedError(
            config['terraform_config']['provider_id'] + ' provider is not implemented')

def release(job_indices):
    """
    Release a held job, to allow it to be allocated to a node.
    """
    if not daemon_running():
        print('Warning: the crossqueue daemon is not running')

    config = load_config()
    data_source = dataobjects.DataSource(config['mongodb_uri'])
    if not data_source.is_connected():
        print('Warning: cannot connect to database')
        return
    jobs = data_source.get_jobs()
    for job in jobs:
        job_index = job.job_index
        if job_index in job_indices:
            if job.status == u'held':
                job.status = u'queued'
                job.patch()
                print('job {} released'.format(job_index))
            elif job.status == u'queued':
                print('job {} already released'.format(job_index))
            elif job.status == u'running':
                print('Error - job {} already running'.format(job_index))
            else:
                print('Error - job {} has already completed'.format(job_index))

def jobstat():
    """
    List the status of all jobs.
    """
    if not daemon_running():
        print('Warning: the crossqueue daemon is not running')

    config = load_config()
    now = int(time.time())
    data_source = dataobjects.DataSource(config['mongodb_uri'])
    if not data_source.is_connected():
        print('Warning: cannot connect to database')
        return
    jobs = data_source.get_jobs()
    jobs.sort(key=lambda j: j.job_index)
    if len(jobs) == 0:
        print('Job queue is empty')
        return
    for job in jobs:
        job_index = job.job_index
        job_status = job.status
        if job_status != 'nascent':
            if job_status in ['queued', 'allocated', 'held']:
                timespan = now - job.submit_time
            elif job_status  in ['running', 'cancelling']:
                timespan = now - job.start_time
            elif job_status in ['cancelled', 'completed', 'failed']:
                timespan = now - job.end_time
            else:
                timespan = 0

            scriptfile = op.basename(job.scriptfile)
            timespan = int(timespan)
            hrs = timespan // 3600
            mins = (timespan - 3600 * hrs) // 60
            secs = timespan - 3600 * hrs - 60 * mins
            printtime = 'for {:02d}:{:02d}:{:02d}'.format(hrs, mins, secs)
 
            print(job_index, scriptfile, job_status, printtime)

def purge():
    """
    Delete all completed, failed, or cancelled jobs from the database. This can be
    called from the command line.
    """
    config = load_config()
    data_source = dataobjects.DataSource(config['mongodb_uri'])
    if not data_source.is_connected():
        logger.warning('purge: cannot connect to database')
        return
    completed_jobs = data_source.get_jobs(status=u'completed')
    for job in completed_jobs:
        logger.info('purge: purging completed job {}'.format(job.job_index))
        job.delete()
    failed_jobs = data_source.get_jobs(status=u'failed')
    for job in failed_jobs:
        logger.info('purge: purging failed job {}'.format(job.job_index))
        job.delete()
    cancelled_jobs = data_source.get_jobs(status=u'cancelled')
    for job in cancelled_jobs:
        logger.info('purge: purging cancelled job {}'.format(job.job_index))
        job.delete()

def terminate(node_ids):
    """
    Set the status of the supplied nodes to 'terminating', which will trigger its
    deletion at the next daemon cycle.
    """
    config = load_config()
    data_source = dataobjects.DataSource(config['mongodb_uri'])
    if not data_source.is_connected():
        print('Error: Cannot connect to database')
        return
    all_nodes = data_source.get_nodes()
    for n in all_nodes:
        if n.node_id in node_ids:
            n.status = 'terminating'
            print('Node {} signalled to terminate'.format(n.node_id))

def rescale():
    """
    Rescale the cluster to the correct number of nodes - delete unnecessary nodes
    once they have been unused for too long, and launch new nodes (up to a total of
    max_nodes) if jobs are waiting.
    """
    config = load_config()
    deployment = terraform.Deployment(op.join(configdir, 'terraform'))
    max_nodes = int(config['max_nodes'])

    data_source = dataobjects.DataSource(config['mongodb_uri'])
    if not data_source.is_connected():
        logger.warning('Rescale: Cannot connect to database')
        return
    queued_jobs = data_source.get_jobs(status=u'queued')
    queued_jobs.sort(key=lambda job: job.submit_time)

    all_nodes = data_source.get_nodes()
    existing_node_names = [n.node_id for n in all_nodes]
    idle_nodes = [node for node in all_nodes if node.status in [u'idle']]
    launching_nodes = [node for node in all_nodes if node.status in [u'launching']]
    dead_nodes = [node for node in all_nodes if node.status in [u'dead', u'terminating', u'terminated']]
    busy_nodes = [node for node in all_nodes if node.status in [u'busy', u'allocated', u'draining']]

    n_busy = len(busy_nodes)
    n_queued = len(queued_jobs)
    n_launching = len(launching_nodes)

    now = time.time()
    retained_idle_nodes = []
    for n in idle_nodes:
        if now - n.last_busy > 60:
            logger.info('rescale: terminate unwanted idle node {}'.format(n.node_id))
            n.delete()
        else:
            retained_idle_nodes.append(n)
    idle_nodes = retained_idle_nodes
    n_idle = len(idle_nodes)

    for n in dead_nodes:
        logger.info('rescale: delete dead node {}'.format(n.node_id))
        n.delete()
    
    if (n_launching + n_busy + n_queued + n_idle) > 0:
        logger.info('rescale: {} launching nodes'.format(n_launching))
        logger.info('rescale: {} idle nodes'.format(n_idle))
        logger.info('rescale: {} busy nodes'.format(n_busy))
        logger.info('rescale: {} queued jobs'.format(n_queued))
    
    new_nodes = [] 
    for n in busy_nodes + launching_nodes + idle_nodes:
        logger.info('rescale: retain node {}'.format(n.node_id))
        new_nodes.append(n)
    
    n_new_wanted = n_queued - n_launching - n_idle
    n_new_available = max_nodes - n_busy - n_launching - n_idle
    if n_new_available > 0 and n_new_wanted > 0:
        n_new = min(n_new_wanted, n_new_available)
        logger.info('rescale: create {} fresh nodes'.format(n_new))
        i_new = 0
        j = -1
        while i_new < n_new:
            j += 1
            test_name = 'jobrunner{}'.format(j)
            if test_name not in existing_node_names:
                node = data_source.node(test_name)
                node.properties = config['node_defaults']
                logger.info('rescale: target requirements {}'.format(queued_jobs[i_new].requirements))
                node_properties = node.properties
                node_properties.update(queued_jobs[i_new].requirements)
                node.properties = node_properties
                #for r in queued_jobs[i_new].requirements:
                #    node.properties[r] = queued_jobs[i_new].requirements[r]
                logger.info('rescale: new node {} properties {}'.format(test_name, node.properties))
                node.status = u'launching'
                i_new += 1
                new_nodes.append(node)

    config['terraform_config']['node_names'] = [n.node_id for n in new_nodes]
    for n in new_nodes:
        logger.info('rescale: new_nodes {}: {}'.format(n.node_id, n.properties))
    config['terraform_config']['node_types'] = {n.node_id: n.properties['instance_type'] for n in new_nodes}
    config['terraform_config']['image_names'] = {n.node_id: n.properties['image_name'] for n in new_nodes}
    if config['terraform_config']['provider_id'] == 'gcp':
        config['terraform_config']['accelerator_types'] = {n.node_id: n.properties['accelerator_type'] for n in new_nodes}
        config['terraform_config']['accelerator_counts'] = {n.node_id: n.properties['accelerator_count'] for n in new_nodes}
    elif config['terraform_config']['provider_id'] == 'aws':
        config['terraform_config']['image_owners'] = {n.node_id: n.properties['image_owner'] for n in new_nodes}
    save_config(config)
    if config == deployment.config():
        return
    
    deployment.apply(config=config['terraform_config'])
    if deployment.status == "OK":
        data = deployment.outputs()
        public_ips = data['jobrunner-ip']['value']
        for node, public_ip in zip(new_nodes, public_ips):
            node.public_ip = public_ip
            logger.info('rescale: node {} public_ip={}'.format(node.node_id, node.public_ip))
    else:
        logger.warning('rescale: deployment update failed')
        logger.warning('rescale: STDOUT={}'.format(deployment.stdout))
        logger.warning('rescale: STDERR={}'.format(deployment.stderr))
        data = deployment.outputs()
        public_ips = data['jobrunner-ip']['value']
        logger.warning('rescale: public_ips = {}'.format(public_ips))

def azure_copy_structure(local_fs, conn_str, sharename):
    """
    Helper function for Azure filesystem: create a copy of the directory structure
    from local_fs in the existing Azure share with name ``sharename`` using the
    provided connection string.
    """
    from azure.storage.fileshare import ShareClient as AzShare
    from azure.core.exceptions import ResourceExistsError
    for dir_path in local_fs.walk.dirs(search='breadth'):
        try:
            AzShare.from_connection_string(conn_str=conn_str,
                                           share_name=sharename).get_directory_client(
                directory_path=dir_path[1:]).create_directory()
        except ResourceExistsError:
            pass

def submit(scriptfile):
    """
    Submit a job from a scriptfile.
    """
    if not daemon_running():
        print('Warning: the crossqueue daemon is not running')

    config = load_config()
    if config['terraform_config']['provider_id'] == 'aws':
        from fs_s3fs import S3FS as BUCKETFS
    elif config['terraform_config']['provider_id'] == 'gcp':
        from fs_gcsfs import GCSFS as BUCKETFS
    elif config['terraform_config']['provider_id'] == 'azure':
        from azure.storage.fileshare import ShareFileClient as AzFile
        from azure.core.exceptions import ResourceNotFoundError
    else:
        raise NotImplementedError(
            config['terraform_config']['provider_id'] + ' provider is not implemented')
    bucket_name = config['terraform_config']['bucket_name']
    data_source = dataobjects.DataSource(config['mongodb_uri'])
    if not data_source.is_connected():
        print('Warning: cannot connect to database')
        return
    metadata = data_source.document('metadata', 'metadata')
    metadata.put('bucket_name', bucket_name)
    metadata.update()
    local_root_dir = config['local_root_dir']
    local_fs = OSFS(local_root_dir)
    # Copy local structure to bucket
    if config['terraform_config']['provider_id'] in ['aws', 'gcp']:
        bucket_fs = BUCKETFS(bucket_name=bucket_name)
        copy_structure(local_fs, bucket_fs)
    elif config['terraform_config']['provider_id'] == 'azure':
        conn_str = config['AZURE_STORAGE_CONNECTION_STRING']
        azure_copy_structure(local_fs, conn_str, SHARE_NAME)
    else:
        raise NotImplementedError(
            config['terraform_config']['provider_id'] + ' provider is not implemented')

    stagefiles = []
    requirements = {}
    with open(scriptfile) as f:
        for line in f.readlines():
            if line[:3] == '#XQ':
                w = line.split()
                if len(w) == 3:
                    if w[1] == 'stage':
                        stagefile = op.relpath(op.abspath(w[2]), local_root_dir)
                        stagefiles.append(stagefile)
                    elif w[1] in ['instance_type', 'accelerator_type', 'image_name']:
                        requirements[w[1]] = w[2]

    if 'accelerator_type' in requirements:
        requirements['accelerator_count'] = 1
    job = data_source.job()
    job.scriptfile = op.relpath(op.abspath(scriptfile), local_root_dir)
    jobfiles = [job.scriptfile] + stagefiles
    job.jobfiles = jobfiles
    job.requirements = requirements
        
    for jobfile in jobfiles:
        if config['terraform_config']['provider_id'] in ['aws', 'gcp']:
            copy_file_if_newer(local_fs, jobfile, bucket_fs, jobfile)
            info = bucket_fs.getinfo(jobfile, namespaces=['details'])
            local_fs.settimes(jobfile, modified=info.modified)
        elif config['terraform_config']['provider_id'] == 'azure':
            file_client = AzFile.from_connection_string(conn_str=conn_str,
                                                        share_name=SHARE_NAME,
                                                        file_path=jobfile)
            # Check whether file exists - if not, then copy
            # If it exists, get timestamp, and compare to timestamp of local file.
            # If local is newer than remote, then copy, then get its modified timestamp
            # and set the local file's modified time to match
            try:
                remote_timestamp = file_client.get_file_properties()[
                    'last_modified'].timestamp()
                local_timestamp = local_fs.getinfo(jobfile,
                                                   namespaces=[
                                                       'details']).modified.timestamp()
                if remote_timestamp < local_timestamp:
                    file_client.delete_file()
                    with local_fs.open(jobfile, "rb") as source_file:
                        file_client.upload_file(source_file)
                    new_timestamp = file_client.get_file_properties()['last_modified']
                    local_fs.settimes(jobfile, modified=new_timestamp)
            except ResourceNotFoundError:
                # Else, compare the timestamp of the files - if the remote is older
                # than local then upload it to remote anew
                with local_fs.open(jobfile, "rb") as source_file:
                    file_client.upload_file(source_file)
                new_timestamp = file_client.get_file_properties()[
                    'last_modified']
                local_fs.settimes(jobfile, modified=new_timestamp)
        else:
            raise NotImplementedError(
                config['terraform_config']['provider_id'] + ' provider is not implemented')
    job.submit()
    print('job {} submitted'.format(job.job_index))
