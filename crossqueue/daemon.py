"""
This module defines the work of the daemon. The daemon can be interacted with by the
user using ``xq daemon {start,stop,log}``
"""
import sys
import os
import os.path as op
import time
import logging
import daemon
import subprocess
from daemon import pidfile
from crossqueue import commands
from crossqueue.config import load_config, configdir

# adapted from:
# https://stackoverflow.com/questions/13106221/how-do-i-set-up-a-daemon-with-python-daemon
#

def run(logf):
    """
    This does the "work" of the daemon
    """

    logger = logging.getLogger('xq_daemon')
    logger.setLevel(logging.INFO)

    fh = logging.FileHandler(logf)
    fh.setLevel(logging.INFO)

    formatstr = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    formatter = logging.Formatter(formatstr)

    fh.setFormatter(formatter)

    logger.addHandler(fh)

    config = load_config()
    while 'mongodb_uri' not in config:
        time.sleep(10)
        config = load_config()
    try:
        index = commands.downsyncd()
        while True:
            commands.allocate()
            index = commands.downsyncd(index=index)
            commands.rescale()
            time.sleep(30)
    except Exception as e:
        logger.error(str(e))
        raise

def start_daemon():
    """
    This launches the daemon in its context
    """

    daemondir = op.join(configdir, 'daemon')
    if not op.exists(daemondir):
        os.mkdir(daemondir)
    pidf = op.join(daemondir, 'xq-daemon.pid')
    logf = op.join(daemondir, 'xq-daemon.log')
    if op.exists(logf):
        os.rename(logf, logf + '.old')
    workdir = op.join(daemondir, 'work')
    if not op.exists(workdir):
        os.mkdir(workdir)
    with daemon.DaemonContext(
        working_directory=workdir,
        umask=0o002,
        pidfile=pidfile.TimeoutPIDLockFile(pidf),
        stdin=sys.stdin,
        stdout=sys.stdout,
        stderr=sys.stderr,
        ) as context:
        run(logf)

def stop_daemon():
    """
    Stop the autoscaling daemon
    """

    daemondir = op.join(configdir, 'daemon')
    pidf = op.join(daemondir, 'xq-daemon.pid')
    if not op.exists(pidf):
        return
    with open(pidf) as f:
        pid = f.readline()
    command = 'kill {}'.format(pid)
    result = subprocess.run(command, shell=True)

def daemon_running():
    """
    Determine if the daemon is running or not.

    Returns:
        Bool: True if running, False if not.
    """

    daemondir = op.join(configdir, 'daemon')
    pidf = op.join(daemondir, 'xq-daemon.pid')
    return op.exists(pidf)

def daemon_log():
    """
    Return the contents of the daemon log file

    Returns:
        bytes: contents of the daemon log file
    """

    daemondir = op.join(configdir, 'daemon')
    logf = op.join(daemondir, 'xq-daemon.log')
    with open(logf) as f:
        data = f.read()
    return data

