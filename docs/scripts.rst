=======
Scripts
=======

**************
Initialisation
**************
.. autoprogram:: xq-init:parser
   :prog: xq-init

******************
The ``xq`` command
******************
.. autoprogram:: xq:parser
   :prog: xq
