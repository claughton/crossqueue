***********************
Running your first jobs
***********************

This page offers some guidance on running your first jobs. More detailed instructions
on the usage of all of the ``xq`` commands can be found in :doc:`Scripts <scripts>`.

==============
Run a test job
==============
Create a directory under your local "root" directory, and in it create a job submission
file ``hello.sh``::

    #!/bin/sh
    echo 'hello world' > output.dat

Submit the job::

    xq submit hello.sh

Follow the job progress with ``xq jobstat`` and ``xq nodestat``. Things will be a bit
sluggish to start with, as a node has to be launched before your job can be run on it
but you should be able to see the job in the queue, the node being launched, the job
being run on the node, the file ``output.dat`` returned to your working directory, and
the node then shut down.

=============
Staging files
=============
*Crossqueue*'s daemon provides a "one-way mirror' for your filesystem: output files
created on the worker nodes are automatically synced back to your local machine, but
local input files required to be present on the worker node at the start of a job need
to be specified explicitly in the job script. This is done using ``#XQ stage``
directives, e.g.::

    #!/bin/sh
    #XQ stage input.dat
    wc -l input.dat > linecount.dat

The file ``linecount.dat`` is an output file and so will be automatically transferred
back from the worker node to the current directory.

If you need to stage multiple input files, you can give the ``#XQ stage`` directive
multiple times.

=========================
Setting node requirements
=========================
The default instance type, accelerator type, and machine image used to run jobs can be
overridden with other ``#XQ`` directives in the script file, e.g.::

    #!/bin/sh
    #XQ stage protein.tpr
    #XQ instance_type c5.xlarge
    #XQ image_name self:my_gromacs_ami

    gmx mdrun -deffnm protein

While in AWS certain instance types have a GPU already "attached" (e.g. "g4dn.xlarge")
in GCP you need to explicitly attach a GPU accelerator if you want one, e.g.::

    #!/bin/sh
    #XQ stage protein.tpr
    #XQ instance_type n1-standard-4
    #XQ accelerator_type nvidia-tesla-t4

    gmx mdrun -deffnm protein
