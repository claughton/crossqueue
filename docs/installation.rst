************
Installation
************
=============
Prerequisites
=============
--------------
Python Version
--------------
*Crossqueue* requires Python 3.7 or higher. No version of Python 2 is supported.

-------------
Configuration
-------------
*Crossqueue* supports AWS, Azure, and GCP. Depending on which of these providers
you plan to use, some preliminary configuration is required:

^^^
AWS
^^^
It is assumed that you have done what is required to give you programmatic access
to your AWS account. This will involve generating your *AWS AccessKey ID* and
*Secret Access Key*, and installing them with ``aws configure``.

In addition you need to make sure your account has the following permissions::

    Amazon EC2FullAccess
    Amazon S3FullAccess

^^^^^
Azure
^^^^^
Log in with the Azure CLI to your Azure account, setting the active subscription
to the correct id. It is assumed that you have an existing resource group
``xq-resource-group`` containing a machine image called ``XQJobrunnerImage``.
This image can be created by following the instructions in :doc:`Packer <packer>`.
To point to another resource group, set the environment variable::

    export AZURE_RESOURCE_GROUP_NAME=<name of resource group>

^^^
GCP
^^^
You need to have downloaded a .json file with your service account credentials - see
`here <https://cloud.google.com/iam/docs/creating-managing-service-account-keys>`_
for details. Then you need to decide on an availability zone for your cluster - bear
in mind that this will affect the range of instance types (particularly GPU
accelerators) you will be able to launch. With these in hand, create two environment
variables::

    export GOOGLE_APPLICATION_CREDENTIALS=<path to credentials file>
    export GOOGLE_DEFAULT_AVAILABILITY_ZONE=<availability zone>

Finally, ensure that you have enabled the Compute Engine API for the project named in
your service account credentials.

---------
Terraform
---------
*Crossqueue* uses `Terraform <https://www.terraform.io>`_ to do the heavy lifting of
cloud infrastructure creation and management. Before you can use *Crossqueue* you
must install terraform according to their instructions. Once you can run ::

    terraform -version

you have done enough.

---
SSH
---
You will need an ssh public key (e.g., ``$HOME/.ssh/id_rsa.pub``). If you don't
already have this, use ``ssh-keygen`` to make it.

Create another environment variable with the location of this file::

    export SSH_PUBLIC_KEY=<path to id_rsa.pub or equivalent>

------------------
A "root" directory
------------------
*Crossqueue* will automatically mirror files below a chosen directory in your local
filesystem with the cloud instances. Only files and directories below this point
will be visible to the cloud instances. For security and performance issues we
recommend you choose or create this somewhere below your home directory.

Create a final environment variable corresponding to this::

    export XQ_ROOT_DIRECTORY=<path to xq root directory>

=====================================
Install the Crossqueue Python Package
=====================================
*Crossqueue* is not currently in `pypi <https://pypi.org>`_ so to install it use::

    pip install git+https://bitbucket.org/claughton/crossqueue.git

If all goes smoothly, you can then check the installation is OK by running ``xq -h``::

    usage: xq [-h] [-V] {start,shutdown,restart,filestat,jobstat,nodestat,submit,hold,release,cancel,delete,purge,terminate,configure,daemon,allocate,downsync,remove,rescale} ...

    XQ: simple queueing system for AWS, Azure, and GCP. This is the main script for interacting with the system. Use any of the subcommands below.

    optional arguments:
      -h, --help            show this help message and exit
      -V, --version         show program's version number and exit

    subcommands:
      {start,shutdown,restart,filestat,jobstat,nodestat,submit,hold,release,cancel,delete,purge,terminate,configure,daemon,allocate,downsync,remove,rescale}
        start               create cloud resources
        shutdown            terminate and delete all resources
        restart             recreate cloud resources
        filestat            list contents of bucket file system
        jobstat             list status of jobs
        nodestat            list status of nodes
        submit              submit a job
        hold                hold a job
        release             release a held job
        cancel              cancel a running job
        delete              delete a job
        purge               purge all completed, failed or cancelled jobs
        terminate           terminate one or more nodes
        configure           get or set xq parameters
        daemon              control the xq daemon
        allocate            (debug) manually trigger allocation of all jobs
        downsync            (debug) manually trigger filesystem sync
        remove              (debug) remove files from local and bucket filesystems
        rescale             (debug) manually trigger rescaling of cluster
