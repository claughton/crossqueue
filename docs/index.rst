.. Crossqueue documentation master file, created by
   sphinx-quickstart on Tue Mar 30 12:03:43 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

##########
Crossqueue
##########

*Crossqueue* is part of `Project Crossbow <https://bitbucket.org/claughton/crossbow>`_. It allows you to create a pool
of instances in the cloud and then send jobs to them using a familiar batch queue type interface. *Crossqueue* handles
allocation of the jobs to instances, instance autoscaling, and file transfer.

Currently *Crossqueue* supports `Amazon Web Services <https://aws.amazon.com>`_,
`Microsoft Azure <https://azure.microsoft.com>`_ and `Google Cloud Platform <https://cloud.google.com>`_.

********
 Authors
********
* Christian Suess
* Charlie Laughton charles.laughton@nottingham.ac.uk
* Sam Cox

***************
Acknowlegements
***************
EPSRC Grant `EP/P011993/1 <https://gow.epsrc.ukri.org/NGBOViewGrant.aspx?GrantRef=EP/P011993/1>`_


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   config_and_start
   first_job
   packer
   api
   scripts
   development

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
