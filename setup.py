from setuptools import setup, find_packages
import re

VSRE = r"^__version__ = ['\"]([^'\"]*)['\"]"
VERSIONFILE = "crossqueue/_version.py"
verstrline = open(VERSIONFILE, "rt").read()
mo = re.search(VSRE, verstrline, re.M)
if mo:
    verstr = mo.group(1)
else:
    raise RunTimeError("Unable to find version string in {}.".format(VERSIONFILE))

with open("README.rst", "r") as f:
    long_description = f.read()

setup(
    name = 'crossqueue',
    version = verstr,
    author = 'Charlie Laughton',
    author_email = 'charles.laughton@nottingham.ac.uk',
    description = 'A simple cloud-based job queue system',
    long_description = long_description,
    long_description_content_type='text/markdown',
    url = 'https://bitbucket.org/claughton/crossqueue',
    classifiers = [
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: BSD License",
        "Development Status :: 3 - Alpha",
    ],
    packages = find_packages(),
    scripts = [
        'scripts/xq',
        'scripts/jobrunner',
        'scripts/xq-init',
    ],
    install_requires = [
        'pyyaml',
        'pymongo',
        'boto3',
        'python-daemon',
        'fs-s3fs',
        'fs-gcsfs',
        'azure-storage-file-share',
    ],
)
